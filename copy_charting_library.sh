#!/bin/sh

remove_if_directory_exists() {
	if [ -d "$1" ]; then rm -Rf "$1"; fi
}

case "$1" in
	"unstable")
		BRANCH="unstable";;
	*)
		BRANCH="master";;
esac

if [ "$GITHUB_TRADINGVIEW_TOKEN" != '' ]
then
	REPOSITORY="https://${GITHUB_TRADINGVIEW_TOKEN}@github.com/tradingview/charting_library.git"
else
	REPOSITORY='https://github.com/tradingview/charting_library/'
fi

LATEST_HASH=$(git ls-remote $REPOSITORY $BRANCH | grep -Eo '^[[:alnum:]]+')
# LATEST_HASH="8a36b10"

remove_if_directory_exists "$LATEST_HASH"

git clone -q --depth 1 -b "$BRANCH" $REPOSITORY "$LATEST_HASH"

remove_if_directory_exists "public/charting_library"
remove_if_directory_exists "public/datafeeds"

cp -r "$LATEST_HASH/charting_library" public
cp -r "$LATEST_HASH/datafeeds" public

remove_if_directory_exists "$LATEST_HASH"