import { RouteRecordRaw } from 'vue-router'

const name = 'explorer'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/blocks',
    name: 'blocks',
    component: () => import('~/explorer/pages/Blocks.vue'),
    meta: { title: 'Recent Blocks' }
  },
  {
    path: '/b/:block(\\d+)',
    name: 'block',
    component: () => import('~/explorer/pages/Block.vue'),
    props: true,
    meta: { title: 'Block: {block}' }
  },
  {
    path: '/tx/:txId([a-z0-9]{40})',
    name: 'transaction',
    component: () => import('~/explorer/pages/Transaction.vue'),
    props: true,
    meta: { title: 'Tx: {txId}' }
  },
  {
    path: '/@:account([a-z][a-z0-9-.]{2,15})',
    name: 'account',
    component: () => import('~/explorer/pages/Account.vue'),
    beforeEnter: (to, from, next) => {
      if (to.params.account !== `${to.params.account}`.toLowerCase()) {
        next(to.fullPath.toLowerCase())
      }
      next()
    },
    props: true
  },
  {
    path: '/@:author([a-z][a-z0-9-.]{2,15})/:permlink',
    name: 'post',
    component: () => import('~/explorer/pages/Post.vue'),
    props: true
  },
  {
    path: '/:parent/@:author([a-z][a-z0-9-.]{2,15})/:permlink',
    redirect: to => ({ name: 'post', params: { author: to.params.author, permlink: to.params.permlink } })
  },
  {
    path: '/witnesses',
    name: 'witnesses',
    component: () => import('~/explorer/pages/Witnesses.vue'),
    props: true,
    meta: { title: 'Witnesses' }
  },
  {
    path: '/witnesses/hive-engine',
    name: 'witnesses-hive-engine',
    component: () => import('~/explorer/pages/WitnessesHiveEngine.vue'),
    props: true,
    meta: { title: 'Hive Engine Witnesses' }
  },
  {
    path: '/proposals',
    name: 'proposals',
    component: () => import('~/explorer/pages/Proposals.vue'),
    props: true,
    meta: { title: 'Proposals' }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_EXPLORER === 'false' ? false : true, // enabled by default
  routes: routes
}
