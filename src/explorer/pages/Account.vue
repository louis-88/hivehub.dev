<template>
  <div>
    <AccountHeader :name="account" :account="accountData"></AccountHeader>

    <header class="pt-6 pb-6">
      <div class="max-w-7xl mx-auto flex">
        <h1 class="text-3xl font-bold grow">Transactions</h1>

        <Filters @change="handleFilterChange" @clearFilters="clearFilters" :filters="filters"></Filters>
        <span class="mx-1"></span>
        <TabSwitch :tabs="tabs"></TabSwitch>
      </div>
    </header>

    <LoadingPanel v-if="isLoadingTransactions"></LoadingPanel>
    <div v-else class="bg-white dark:bg-slate-800 rounded-lg shadow ring-1 ring-black dark:ring-slate-700 ring-opacity-5 px-5 py-3 sm:px-6">
      <ul role="list" :class="{ 'divide-y divide-gray-200 dark:divide-slate-600': !settingsStore.settings.transactionsRendering }">
        <li v-for="trxRecord in filteredTransactions" :key="trxRecord[0]">
          <TransactionRecord :trx="trxRecord[1]" :timestamp="trxRecord[1].timestamp"></TransactionRecord>
        </li>
      </ul>
    </div>

    <Pagination class="mt-4" :pages="lastPage" :current="currentPage" :last="currentPage === lastPage" @page-changed="onPageChange"></Pagination>

    <AccountRawDataModal :account="accountData" :show="showAdvanced" @close="router.push(`/@${account}`)"></AccountRawDataModal>
  </div>
</template>

<script setup lang="ts">
  import { AppliedOperation } from '@hiveio/dhive'
  import { ShallowRef } from 'vue'
  import { getAccountHistory, getAccounts } from '~/common/api/hiveApi'
  import { useSettingsStore } from '~/stores/settings'
  import appsConfig from '~/explorer/helpers/apps'
  import { splitAndCapitalize } from '~/common/helpers/formatter'
  import { operations } from '~/common/helpers/operations'

  const settingsStore = useSettingsStore()

  const props = defineProps<{
    account: string
  }>()

  const tabs = computed(() => [
    { name: 'Hive', to: `/@${props.account}`, current: route.path === `/@${props.account}`, enabled: true },
    { name: 'Hive Engine', to: `/@${props.account}/hive-engine`, current: route.path === `/@${props.account}/hive-engine`, enabled: false },
    { name: 'Splinterlands', to: `/@${props.account}/splinterland`, current: route.path === `/@${props.account}/splinterlands`, enabled: false }
  ])

  const route = useRoute()
  const router = useRouter()

  const account = props.account.startsWith('@') ? props.account.substring(1) : props.account

  console.log('Loading account transactions: ', account)

  const accountResults = await getAccounts([account])
  if (!accountResults || accountResults.length !== 1) {
    throw new Error(`Account not found: ${account}`)
  }

  const accountData = accountResults[0]

  const showAdvanced = computed(() => route.query.show === 'advanced')

  // fetch
  const isLoadingTransactions = ref(false)
  const fetchTransactions = async (start: number, limit?: number): Promise<[number, any][]> => {
    try {
      isLoadingTransactions.value = true
      const effectiveLimit = Math.min(limit || pageSize, start < 0 ? limit || pageSize : start)
      const results = await getAccountHistory(account, start, effectiveLimit)
      return results
        .reverse()
        .filter((r: [number, AppliedOperation], index: number) => start === -1 || r[0] <= start)
        .map((r: [number, AppliedOperation]): [number, any] => [
          r[0],
          {
            block_num: r[1].block,
            operations: [r[1].op],
            transaction_id: r[1].trx_id,
            timestamp: r[1].timestamp,
            virtual: r[1].virtual_op
          }
        ])
    } finally {
      isLoadingTransactions.value = false
    }
  }

  // pagination
  const pageSize = 100
  const currentPage = computed(() => (route.query.page ? parseInt(`${route.query.page}`) : 1))

  // this code is required so we have the total number of entries and we know how many pages to display
  const fetched: ShallowRef<[number, any][]> = shallowRef([])
  let latestHistoryRecord = -1
  if (currentPage.value === 1) {
    fetched.value = await fetchTransactions(-1)
    latestHistoryRecord = fetched.value[0][0]
  } else {
    const latestHistoryTransaction = await fetchTransactions(-1, 1)
    latestHistoryRecord = latestHistoryTransaction[0][0]
    fetched.value = await fetchTransactions(latestHistoryRecord - (currentPage.value - 1) * pageSize)
  }
  const lastPage = Math.ceil(latestHistoryRecord / pageSize)

  const transactions = computed(() => {
    const results: any[] = []

    for (let activity of fetched.value) {
      const matched = results.find(
        (trx: any) => !trx[1].transaction_id.match(/^0+$/) && !trx[1].virtual && trx[1].transaction_id === activity[1].transaction_id
      )
      // we found a match -> merge operations and
      if (matched) {
        matched[1].operations = matched[1].operations.concat(activity[1].operations)
        // merged with another trx -> don't add to the result
        continue
      }

      results.push(activity)
    }

    return results
  })

  // all apps
  const allApps = [...appsConfig.map(app => app.name).sort((a, b) => a.localeCompare(b))].map((appName: string) => ({ name: appName, value: appName }))

  // all ops
  const pageOps = [...operations.map(op => op.operation).sort((a, b) => a.localeCompare(b))].map((opsName: string) => ({
    name: splitAndCapitalize(opsName),
    value: opsName
  }))

  // filtering
  const filters = ref({
    text: {
      label: 'Text',
      placeholder: 'Search by text',
      value: ''
    },
    app: {
      label: 'Apps',
      options: [{ name: 'All', value: 'all' }, ...allApps],
      value: 'all'
    },
    operations: {
      label: 'Operations',
      options: [{ name: 'All', value: 'all' }, ...pageOps],
      value: 'all'
    }
  })

  const handleFilterChange = (filterName: string, selectedOption: any) => {
    if (filterName === 'text') {
      filters.value.text.value = selectedOption
    } else {
      for (const option of filters.value[filterName].options) {
        if (selectedOption === option.value) {
          filters.value[filterName].value = option.value
          return
        }
      }
    }
  }

  const clearFilters = (defaultFilters: string) => (filters.value = JSON.parse(defaultFilters))

  const filteredTransactions = computed(() => {
    return transactions.value.filter(tx => {
      // text
      if (filters.value.text.value) {
        const words = filters.value.text.value.split(' ')
        for (let i = 0; i < words.length; i++) {
          if (JSON.stringify(tx).search(words[i]) === -1) {
            return false
          }
        }
      }

      // app
      if (filters.value.app.value !== 'all') {
        const app = appsConfig.find(app => app.match(tx[1].operations[0]))
        if (!app || app.name !== filters.value.app.value) {
          return false
        }
      }

      // operations
      if (filters.value.operations.value !== 'all' && tx[1].operations[0][0] !== filters.value.operations.value) {
        return false
      }
      return true
    })
  })

  const onPageChange = async (page: number) => {
    router.push(`/@${props.account}?page=${page}`)
  }

  watch(
    () => route.query.page,
    async () => {
      fetched.value = await fetchTransactions(currentPage.value === 1 ? -1 : latestHistoryRecord - (currentPage.value - 1) * pageSize)
    }
  )
</script>
