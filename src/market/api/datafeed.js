import { getTradeHistory } from '../../common/api/hiveApi'
import { parseChartData } from '../helpers/chartHelper'

const latestBarDate = ref('')
const allBars = ref([])

const lastBarsCache = new Map()

let dateRefreshTimer = null

const configurationData = {
  supported_resolutions: ['1', '5', '15', '30', '1h', '2h', '4h'],
  // exchanges = [] leads to the absence of the exchanges filter in Symbol Search list
  exchanges: [
    {
      // `exchange` argument for the `searchSymbols` method, if a user selects this exchange
      value: 'Hive core',
      // filter name
      name: 'Hive core',
      // full exchange name displayed in the filter popup
      desc: 'Hive core'
    }
  ],
  // symbols_types = [] leads to the absence of filter types in Symbol Search list
  symbols_types: [
    {
      name: 'crypto',
      // `symbolType` argument for the `searchSymbols` method, if a user selects this symbol type
      value: 'crypto'
    }
  ]
}

async function getAllSymbols() {
  /*
  {
    symbol: actual symbol,
    full_name: used to search
  }
  */
  const allSymbols = [
    {
      symbol: 'HIVE/HBD',
      full_name: 'HIVE/HBD',
      description: 'HIVE / HBD',
      exchange: '',
      type: 'crypto'
    }
  ]

  return allSymbols
}

export default {
  onReady: callback => {
    //console.log('[onReady]: Method call')
    setTimeout(() => callback(configurationData))
  },

  searchSymbols: async (userInput, exchange, symbolType, onResultReadyCallback) => {
    console.log('[searchSymbols]: Method call')
    const symbols = await getAllSymbols()
    const newSymbols = symbols.filter(symbol => {
      const isExchangeValid = exchange === '' || symbol.exchange === exchange
      const isFullSymbolContainsInput = symbol.full_name.toLowerCase().indexOf(userInput.toLowerCase()) !== -1
      return isExchangeValid && isFullSymbolContainsInput
    })
    onResultReadyCallback(newSymbols)
  },

  resolveSymbol: async (symbolName, onSymbolResolvedCallback, onResolveErrorCallback) => {
    //console.log('[resolveSymbol]: Method call', symbolName)
    const symbols = await getAllSymbols()
    const symbolItem = symbols.find(({ full_name }) => full_name === symbolName)
    if (!symbolItem) {
      //console.log('[resolveSymbol]: Cannot resolve symbol', symbolName);
      onResolveErrorCallback('cannot resolve symbol')
      return
    }

    const symbolInfo = {
      ticker: symbolItem.full_name,
      name: symbolItem.symbol,
      description: symbolItem.description,
      type: symbolItem.type,
      session: '24x7',
      timezone: 'Etc/UTC',
      exchange: symbolItem.exchange,
      minmov: 1,
      pricescale: 1000,
      has_intraday: true,
      has_no_volume: false,
      has_weekly_and_monthly: false,
      supported_resolutions: configurationData.supported_resolutions,
      volume_precision: 2
      //data_status: 'streaming',
    }

    //console.log('[resolveSymbol]: Symbol resolved', symbolName);
    onSymbolResolvedCallback(symbolInfo)
  },

  // If the data has been chaced this function won't be called, so chart_series cannot be updated here
  getBars: async (symbolInfo, resolution, periodParams, onHistoryCallback, onErrorCallback) => {
    var { from, to, countBack, firstDataRequest } = periodParams
    //console.log('[getBars]: Method call', symbolInfo, resolution, from, to)

    try {
      // If it's not the first time the same data is requested then is cached by trading view
      if (firstDataRequest == false) {
        onHistoryCallback([], { noData: true })
        return
      }

      const today = new Date()
      const startDate = new Date(today.setDate(today.getDate() - 7)).toISOString()

      const bars = parseChartData(await getTradeHistory(1000, startDate.substring(0, 19)))
      const lastBarDate = bars.at(-1).date
      latestBarDate.value = lastBarDate

      // Get raw data
      //let bars = {}
      //bars = await getTradeHistory()

      if (bars.length == 0) {
        onHistoryCallback([], { noData: true })
        return
      }

      // Convert the data in bar format
      //bars = build_bars(bars, symbolInfo.currency, symbolInfo.exchange != 'Splinterlands')

      // If it's the first time chache the data
      if (firstDataRequest) {
        lastBarsCache.set(symbolInfo.full_name, {
          ...bars[bars.length - 1]
        })
      }
      //console.log(`[getBars]: returned ${bars.length} bar(s)`);
      allBars.value = bars
      onHistoryCallback(bars, {
        noData: false
      })
    } catch (error) {
      console.log('[getBars]: Get error', error)
      onErrorCallback(error)
    }
  },

  subscribeBars: (symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) => {
    dateRefreshTimer = setInterval(async () => {
      const newBars = parseChartData(await getTradeHistory(1000, latestBarDate.value.substring(0, 19))).splice(1)
      if (newBars.length > 0) {
        const newLastBarDate = newBars.at(-1).date
        latestBarDate.value = newLastBarDate
        allBars.value = [...allBars.value, ...newBars]
        newBars.forEach(bar => onRealtimeCallback(bar))
      }
    }, 6000)
  },

  unsubscribeBars: subscriberUID => {
    if (dateRefreshTimer) {
      clearInterval(dateRefreshTimer)
    }
  }
}
