import { acceptHMRUpdate, defineStore } from 'pinia'
import { Ref } from 'vue'
import { updateHiveEndpoint } from '~/common/api/hiveApi'
import { updateHiveEngineEndpoint } from '~/explorer/api/hiveEngineApi'

type AppSettings = {
  apiNode: string
  heApiNode: string
  language: string
  darkMode: boolean
  transactionsRendering: boolean
  jsonRendering: boolean
  jsonFontSize: boolean
}

const defaultSettings: AppSettings = {
  apiNode: 'https://api.hive.blog',
  heApiNode: 'https://api.hive-engine.com',
  language: 'en',
  darkMode: false,
  transactionsRendering: false,
  jsonRendering: true,
  jsonFontSize: false
}

export const useSettingsStore = defineStore('settings', () => {
  const { locale } = useI18n()

  let localStorageJson = {}

  const localStorageItem = localStorage.getItem('settings')
  if (localStorageItem) {
    try {
      localStorageJson = JSON.parse(localStorageItem)
    } catch (error) {
      console.error('Error parsing stored settings', error)
    }
  }

  const propagateSettings = async (newSettings: AppSettings) => {
    updateHiveEndpoint(newSettings.apiNode)
    updateHiveEngineEndpoint(newSettings.heApiNode)

    if (newSettings.darkMode) {
      if (!document.documentElement.classList.contains('dark')) {
        document.documentElement.classList.add('dark')
      }
    } else {
      document.documentElement.classList.remove('dark')
    }

    if (newSettings.language) {
      locale.value = newSettings.language
    }
  }

  const settings: Ref<AppSettings> = ref({
    ...defaultSettings,
    ...localStorageJson
  })

  propagateSettings(settings.value)

  const saveSettings = async (updatedSettings: AppSettings) => {
    propagateSettings(updatedSettings)

    settings.value = updatedSettings
    localStorage.setItem('settings', JSON.stringify(updatedSettings))
  }

  return {
    settings,
    saveSettings
  }
})

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useSettingsStore, import.meta.hot))
