import axios from 'axios'

const ENDPOINT = import.meta.env.VITE_APP_STATS_ENDPOINT || 'https://stats.hivehub.dev'

export const fetchData = async (metric: string, limit: number = 100, offset: number = 0): Promise<any> => {
  return (await axios.get(`${ENDPOINT}/${metric}`, { params: { limit, offset } })).data
}
