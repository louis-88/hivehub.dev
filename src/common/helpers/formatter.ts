import numeral from 'numeral'
import { formatDistance, formatDistanceStrict, formatDuration, format, intervalToDuration } from 'date-fns'

export const truncate = (text: string, maxLength: number = 40, ellipse: string = '…'): string => {
  if (!text || text.length <= maxLength) {
    return text
  }

  if (maxLength < 2) {
    return ellipse
  }

  return `${text.slice(0, maxLength - 1)}${ellipse}`
}

export const capitalize = (text: string): string => {
  return text.charAt(0).toUpperCase() + text.substring(1)
}

export const splitAndCapitalize = (snakeCaseText: string): string => {
  if (!snakeCaseText) {
    return snakeCaseText
  }

  const words = snakeCaseText.match(/[A-Za-z0-9()][A-Za-z0-9()]*/g) || []
  return words.map(word => word.charAt(0).toUpperCase() + word.substring(1)).join(' ')
}

/*
export const splitAndCapitalize = function (snakeCaseText: string) {
  let words = snakeCaseText.split('_')
  return words.map(word => word.charAt(0).toUpperCase() + word.substring(1)).join(' ')
}
*/

export function formatNumber(n: number, format: string = '0,0[.]000') {
  return numeral(n).format(format)
}

export function formatPercentage(n: number) {
  return numeral(n).format('0.00%')
}

export function formatCurrency(source: string | number, format: string, includeToken: boolean = false) {
  if (typeof source === 'string' && includeToken) {
    const parts = source.split(' ')[0]
    if (parts.length === 2) {
      return numeral(parts[0]).format(format) + ' ' + parts[1]
    }
  }

  return typeof source === 'string' ? numeral(source.split(' ')[0]).format(format) : numeral(source).format(format)
}

export const formatISOTimestamp = (isoTimestamp: string) => {
  const date = new Date(isoTimestamp)
  return `${date.toLocaleDateString([], { dateStyle: 'medium' })} ${date.toLocaleTimeString([], { timeZone: 'UTC', timeStyle: 'long' })}`
}

export const formatDate = (date: any) => {
  return format(new Date(date), 'PP')
}

export const formatJson = (json: string) => {
  return JSON.stringify(JSON.parse(json), null, 2)
}

export const timeAgo = (from: Date, to: Date, unit: 'second' | 'minute' | 'hour' | 'day' | 'month' | 'year') => {
  const localUnit = unit ? unit : to.getTime() - from.getTime() > 86400000 ? 'day' : 'hour'
  return formatDistanceStrict(from, to, { unit: localUnit })
}

export const timeAgoWithSeconds = (from: Date, to: Date) => {
  return formatDistance(from, to, { includeSeconds: true })
}

export const timeDifference = (from: Date, to: Date) => {
  const duration = intervalToDuration({
    start: from,
    end: to
  })
  return formatDuration(duration)
}

export const timeSince = (date: Date): string => {
  const dateNotUTC = new Date()
  let dateUTC =
    dateNotUTC.getUTCFullYear() +
    '-' +
    (dateNotUTC.getUTCMonth() < 9 ? '0' + (dateNotUTC.getUTCMonth() + 1) : dateNotUTC.getUTCMonth() + 1) +
    '-' +
    (dateNotUTC.getUTCDate() > 9 ? dateNotUTC.getUTCDate() : `0${dateNotUTC.getUTCDate()}`) +
    // dateNotUTC.getUTCDate() +
    'T' +
    (dateNotUTC.getUTCHours() > 9 ? dateNotUTC.getUTCHours() : `0${dateNotUTC.getUTCHours()}`) +
    ':' +
    (dateNotUTC.getUTCMinutes() > 9 ? dateNotUTC.getUTCMinutes() : `0${dateNotUTC.getUTCMinutes()}`) +
    ':' +
    (dateNotUTC.getUTCSeconds() > 9 ? dateNotUTC.getUTCSeconds() : `0${dateNotUTC.getUTCSeconds()}`)
  let seconds = Math.floor((new Date(dateUTC) - date) / 1000)
  let interval = seconds / 31536000
  if (interval > 1) {
    return Math.floor(interval) + ' years'
  }
  interval = seconds / 2592000
  if (interval > 1) {
    return Math.floor(interval) + ' months'
  }
  interval = seconds / 86400
  if (interval > 1) {
    return Math.floor(interval) + ' days'
  }
  interval = seconds / 3600
  if (interval > 1) {
    return Math.floor(interval) + ' hours'
  }
  interval = seconds / 60
  if (interval > 1) {
    return Math.floor(interval) + ' mins'
  }
  return Math.floor(seconds) + ' secs'
}
